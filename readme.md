## Desafio Leilão de Produtos - Stratum

Um leilão permite que um cliente envie uma sugestão de preço de compra de um produto,
onde por fim vence o cliente que tiver ofertado melhor preço via API RESTful.

# Requirements
PHP >= 5.6.4

# Framework utilizado
Laravel 5.4

# Variaveis Postman

api_url = http://stratum.chavesdev.com.br/

