<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bid extends Model
{
    public $fillable = ['item_id', 'price', 'client_id', 'timestamp'];
}
