<?php

namespace App\Dao;

use App\Models\Client;
use Illuminate\Http\Request;

class ClientDao extends CacheStorage
{
    const KEY = "clients";

    public static function list() : Array {
        $results = parent::get(self::KEY);
        if($results){
            return json_decode($results);
        }
        return [];
    }

    public static function store(Request $request) : Client{
        $client = new Client;
        $client->name = $request->name;
        $client = parent::add(self::KEY, self::list(), $client);

        return $client;
    }
}