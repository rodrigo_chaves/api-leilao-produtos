<?php

namespace App\Dao;

use App\Models\Bid;
use Illuminate\Http\Request;

class BidDao extends CacheStorage
{
    const KEY = "bids";

    public static function list() : Array {
        $results = parent::get(self::KEY);
        if($results){
            return json_decode($results);
        }
        return [];
    }

    public static function store(Request $request) : Bid{
        $bid = new Bid;
        $bid->price = $request->price;
        $bid->client_id = $request->client_id;
        $bid->item_id = $request->item_id;
        $bid->timestamp = time();
        $bid = parent::add(self::KEY, self::list(), $bid);

        return $bid;
    }

    public static function listAll(){
        $bids = self::list();
        $resultBids = [];
        foreach($bids as $bid){
            if(!self::hasItemId($resultBids, $bid->item_id)){
                $resultBid = [
                    'item_id' =>  $bid->item_id,
                    'hits' => self::getHits($bid->item_id),
                    'best_bid' => self::getBestBid($bid->item_id)
                ];
                array_push($resultBids, $resultBid);
            }
        }

        return $resultBids;
    }

    public static function hasItemId($bids, $itemId){
        foreach($bids as $bid){
            if($bid['item_id'] == $itemId){
                return true;
            }
        }
        return false;
    }

    public static function getBestBid($itemId){
        $bids = self::list();
        $bestPrice = 0.0;
        $bestBid = null;
        foreach($bids as $bid){
            if($bid->price > $bestPrice && $bid->item_id == $itemId){
                $bestPrice = $bid->price;
                $bestBid = $bid;
            }
        }

        return $bestBid;
    }

    public static function getHits($itemId){
        $bids = self::list();
        $counter = 0;
        foreach($bids as $bid){
            if($bid->item_id == $itemId){
                $counter++;
            }
        }

        return $counter;
    }

    public static function getTotalHits($bids){
        $counter = 0;
        foreach($bids as $bid){
            $counter+= $bid['hits'];
        }
        return $counter;
    }

}