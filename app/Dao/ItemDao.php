<?php

namespace App\Dao;

use App\Models\Item;
use Illuminate\Http\Request;

class ItemDao extends CacheStorage
{

    const KEY = "items";

    public static function list() : Array {
        $results = parent::get(self::KEY);
        if($results){
            return json_decode($results);
        }
        return [];
    }

    public static function store(Request $request) : Item{
        $item = new Item;
        $item->description = $request->description;
        $item = parent::add(self::KEY, self::list(), $item);

        return $item;
    }

    public static function update(Item $item){}
    
    public static function destroy(Item $item){}
}
