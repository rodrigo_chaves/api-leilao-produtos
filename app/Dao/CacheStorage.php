<?php

namespace App\Dao;

use Illuminate\Support\Facades\Cache;

class CacheStorage
{
    protected static function get($key){
        return Cache::get($key);
    }

    protected static function add($key, $list, $item){
        $item->id = 1;
        if(!empty($list)){
            $last = end($list);
            $item->id = (int) $last->id + 1;
        }

        array_push($list, $item);

        self::save($key, json_encode($list));
        return $item;
    }

    protected static function save($key, $value){
        Cache::forever($key, $value);
    }

    protected static function delete($key){
        Cache::forget($key);
    }
}