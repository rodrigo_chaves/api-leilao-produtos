<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Dao\ItemDao;
use Validator;

class ItemsController extends Controller
{
    public function index(){
        return response()->json(ItemDao::list(), 200);
    }

    public function store(Request $request){
        $validation = Validator::make($request->all(),[
            'description' => 'required'
        ]);

        if($validation->fails()){
            return response()->json($validation->errors()->all(), 400);
        }

        $item = ItemDao::store($request);

        return response()->json($item, 200);
    }
}
