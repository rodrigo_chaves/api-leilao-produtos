<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Dao\BidDao;

class StatsController extends Controller
{
    public function index(){
        $bids = BidDao::listAll();
        $totalBids = BidDao::list();
        $stats = [
            'total_bids' => count($totalBids),
            'total_hits' => BidDao::getTotalHits($bids),
            'bids' => $bids
        ];

        return response()->json($stats, 200);
    }
}
