<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Dao\ClientDao;
use Validator;

class ClientsController extends Controller
{
    public function index(){
        return response()->json(ClientDao::list(), 200);
    }

    public function store(Request $request){
        $validation = Validator::make($request->all(),[
            'name' => 'required'
        ]);

        if($validation->fails()){
            return response()->json($validation->errors()->all(), 400);
        }

        $client = ClientDao::store($request);

        return response()->json($client, 200);
    }
}
