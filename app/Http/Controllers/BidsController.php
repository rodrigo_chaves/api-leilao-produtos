<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Dao\BidDao;
use Validator;

class BidsController extends Controller
{
    public function index(){
        return response()->json(BidDao::list(), 200);
    }

    public function store(Request $request){
        $validation = Validator::make($request->all(),[
            'price' => 'required',
            'item_id' => 'required',
            'client_id' => 'required'
        ]);

        if($validation->fails()){
            return response()->json($validation->errors()->all(), 400);
        }

        $bid = BidDao::store($request);

        return response()->json($bid, 200);
    }
}
