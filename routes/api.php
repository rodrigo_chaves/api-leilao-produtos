<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('items')->group(function(){
    Route::get('/', 'ItemsController@index');
    Route::post('/', 'ItemsController@store');
});

Route::prefix('clients')->group(function(){
    Route::get('/', 'ClientsController@index');
    Route::post('/', 'ClientsController@store');
});

Route::prefix('bids')->group(function(){
    Route::get('/', 'BidsController@index');
    Route::post('/', 'BidsController@store');
});

Route::get('/stats', 'StatsController@index');
